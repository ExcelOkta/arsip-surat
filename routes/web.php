<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/login');
});

Route::get('/dashboard', 'SuratController@index')->name('dashboard')->middleware('auth');

Route::prefix('surat')->group(function () {
  Route::get('/masuk', 'SuratController@masuk')->name('surat_masuk')->middleware('auth');
  Route::get('/keluar', 'SuratController@keluar')->name('surat_keluar')->middleware('auth');
  Route::get('/kirim', 'SuratController@create')->name('kirim_surat')->middleware('auth');
  Route::get('/baca/{id}', 'SuratController@baca')->name('baca_surat')->middleware('auth');
  Route::post('/kirim', 'SuratController@store')->name('post_mail')->middleware('auth');
  Route::get('/hapus/{id}', 'SuratController@destroy')->name('delete_mail')->middleware('auth');
  Route::get('/arsip/{id}', 'SuratController@arsip')->name('arsip')->middleware('auth');
  Route::get('/lanjut/{id}', 'SuratController@lanjut')->name('lanjut')->middleware('auth');
  Route::post('/cari', 'SuratController@cari')->name('cari_surat')->middleware('auth');
});

Route::prefix('disposisi')->group(function () {
   Route::get('/masuk', 'DisposisiController@masuk')->name('disposisi_masuk')->middleware('auth');
   Route::get('/keluar', 'DisposisiController@keluar')->name('disposisi_keluar')->middleware('auth');
   Route::get('/baca/{id}', 'DisposisiController@baca')->name('disposisi_baca')->middleware('auth');
   Route::get('/hapus/{id}', 'DisposisiController@destroy')->name('disposisi_delete')->middleware('auth');
   Route::post('/kirim', 'DisposisiController@store')->name('disposisi_post')->middleware('auth');
   Route::post('/cari', 'DisposisiController@cari')->name('cari_disposisi')->middleware('auth');
});

Route::prefix('entri')->group(function () {
  // Jenis
  Route::get('/jenis', 'TipeController@index')->name('jenis')->middleware('auth');
  Route::post('/jenis', 'TipeController@store')->name('post_jenis')->middleware('auth');
  Route::get('/jenis/{id}', 'TipeController@destroy')->name('delete_jenis')->middleware('auth');
  Route::put('/jenis/{id}', 'TipeController@update')->name('update_jenis')->middleware('auth');
  Route::post('/jenis/cari', 'TipeController@cari')->name('cari_jenis')->middleware('auth');
  // Anggota
  Route::get('/anggota', 'AnggotaController@index')->name('anggota')->middleware('auth');
  Route::post('/anggota', 'AnggotaController@store')->name('post_anggota')->middleware('auth');
  Route::get('/anggota/{id}', 'AnggotaController@destroy')->name('delete_anggota')->middleware('auth');
  Route::put('/anggota/{id}', 'AnggotaController@update')->name('update_anggota')->middleware('auth');
  Route::post('/anggota/cari', 'AnggotaController@cari')->name('cari_anggota')->middleware('auth');
});

Route::get('/profile','AnggotaController@edit')->name('profile')->middleware('auth');

Route::get('/laporan','SuratController@laporan')->name('laporan')->middleware('auth');
Route::post('/laporan','SuratController@laporan_post')->name('post_laporan')->middleware('auth');

Auth::routes();
