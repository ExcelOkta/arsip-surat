-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Mar 12, 2018 at 03:46 PM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 5.6.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `surat`
--

-- --------------------------------------------------------

--
-- Table structure for table `excel_dispositions`
--

CREATE TABLE `excel_dispositions` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_mail` int(10) UNSIGNED NOT NULL,
  `mail_to` int(10) UNSIGNED NOT NULL,
  `mail_from` int(10) UNSIGNED NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `mark` enum('read','unread') COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('Surat','Arsip') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `excel_dispositions`
--

INSERT INTO `excel_dispositions` (`id`, `id_mail`, `mail_to`, `mail_from`, `description`, `mark`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 2, 'Lanjutkan', 'read', 'Surat', '2018-03-12 07:24:47', '2018-03-12 07:37:24'),
(2, 2, 1, 2, 'Arsipkan', 'unread', 'Arsip', '2018-03-12 07:41:57', '2018-03-12 07:44:20');

-- --------------------------------------------------------

--
-- Table structure for table `excel_logsurat`
--

CREATE TABLE `excel_logsurat` (
  `id` int(10) UNSIGNED NOT NULL,
  `mail_subject` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mail_from` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mail_to` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `excel_logsurat`
--

INSERT INTO `excel_logsurat` (`id`, `mail_subject`, `mail_from`, `mail_to`, `created_at`, `updated_at`) VALUES
(1, 'Pelatihan Tenanga Pelajar', '1', '2', '2018-03-12 14:22:00', NULL),
(2, 'Pelatihan Tenanga Pelajar', '1', '2', '2018-03-12 14:24:22', NULL),
(3, 'Pelatihan Tenanga Pelajar', '1', '2', '2018-03-12 14:37:25', NULL),
(4, 'Pelatihan Tenanga Pelajar', '1', '2', '2018-03-12 14:37:33', NULL),
(5, 'Pelatihan Tenanga Pelajar', '1', '2', '2018-03-12 14:38:27', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `excel_mails`
--

CREATE TABLE `excel_mails` (
  `id` int(10) UNSIGNED NOT NULL,
  `mail_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `from` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mail_from` int(10) UNSIGNED NOT NULL,
  `mail_to` int(10) UNSIGNED NOT NULL,
  `mail_subject` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `file` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_type` int(10) UNSIGNED NOT NULL,
  `mark` enum('read','unread') COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('Surat','Arsip') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `excel_mails`
--

INSERT INTO `excel_mails` (`id`, `mail_code`, `from`, `mail_from`, `mail_to`, `mail_subject`, `description`, `file`, `id_type`, `mark`, `status`, `created_at`, `updated_at`) VALUES
(1, '20/KEMDIKBUD/2018', 'Kemdikbud Depok', 1, 2, 'Pelatihan Tenanga Pelajar', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>asdasdasdas</p>\r\n</body>\r\n</html>', '1520864520_Kemdikbud.pdf', 1, 'read', 'Surat', '2018-03-12 07:22:00', '2018-03-12 07:22:26'),
(2, '20/KEMDIKBUD/2018', 'Kominfo Depok', 1, 2, 'Pelatihan Tenanga Pelajar', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>asdasdas</p>\r\n</body>\r\n</html>', '1520864662_Kemdikbud.pdf', 1, 'read', 'Surat', '2018-03-12 07:24:22', '2018-03-12 07:41:26'),
(5, '0312143827613/SIAS/2018', 'Kemdikbud Depok', 1, 2, 'Pelatihan Tenanga Pelajar', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>asdasdasdas</p>\r\n</body>\r\n</html>', '1520864520_Kemdikbud.pdf', 1, 'read', 'Arsip', '2018-03-12 07:38:27', '2018-03-12 07:41:14');

--
-- Triggers `excel_mails`
--
DELIMITER $$
CREATE TRIGGER `tr_logsurat` AFTER INSERT ON `excel_mails` FOR EACH ROW BEGIN
            INSERT INTO excel_logsurat (`id`, `mail_subject`, `mail_from`, `mail_to`, `created_at`, `updated_at`)
            VALUES (null, NEW.mail_subject, NEW.mail_from, NEW.mail_to, now(), null);
          END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `excel_migrations`
--

CREATE TABLE `excel_migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `excel_migrations`
--

INSERT INTO `excel_migrations` (`id`, `migration`, `batch`) VALUES
(100, '2014_10_12_000000_create_users_table', 1),
(101, '2014_10_12_100000_create_password_resets_table', 1),
(102, '2018_01_12_011128_create_types_table', 1),
(103, '2018_01_12_034638_create_mails_table', 1),
(104, '2018_01_12_034723_create_dispositions_table', 1),
(105, '2018_03_08_123222_create_logsurat_table', 1),
(106, '2018_03_08_123248_create_log_trigger', 1);

-- --------------------------------------------------------

--
-- Table structure for table `excel_password_resets`
--

CREATE TABLE `excel_password_resets` (
  `username` varchar(35) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `excel_types`
--

CREATE TABLE `excel_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `excel_types`
--

INSERT INTO `excel_types` (`id`, `type`, `created_at`, `updated_at`) VALUES
(1, 'Laporan', '2018-03-12 07:19:27', '2018-03-12 07:19:27'),
(2, 'Tugas', '2018-03-12 07:19:29', '2018-03-12 07:19:29'),
(3, 'Pelatihan', '2018-03-12 07:19:32', '2018-03-12 07:19:32');

-- --------------------------------------------------------

--
-- Table structure for table `excel_users`
--

CREATE TABLE `excel_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(35) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `level` enum('Kepala Sekolah','Tata Usaha','Kurikulum','Kesiswaan','Guru') COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `excel_users`
--

INSERT INTO `excel_users` (`id`, `name`, `username`, `password`, `photo`, `level`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Excel Dwi Oktavianto', 'admin', '$2y$10$lGaZOSFAm8hAVFrzuLCRt.lMaOgY7Q2eiBShQ.7oUJgl3oDrXMPYG', 'profile.jpg', 'Tata Usaha', NULL, '2018-03-12 07:19:01', '2018-03-12 07:19:01'),
(2, 'Ramadin Tarigan', 'kepalasekolah', '$2y$10$wLOABykk0iQoA2E6lKdGw.2Tjq5cHeLr0Su8c34uWDrHMyDfV1eXm', 'profile.jpg', 'Kepala Sekolah', NULL, '2018-03-12 07:21:21', '2018-03-12 07:21:21');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `excel_dispositions`
--
ALTER TABLE `excel_dispositions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `dispositions_id_mail_foreign` (`id_mail`),
  ADD KEY `dispositions_mail_to_foreign` (`mail_to`);

--
-- Indexes for table `excel_logsurat`
--
ALTER TABLE `excel_logsurat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `excel_mails`
--
ALTER TABLE `excel_mails`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mails_id_type_foreign` (`id_type`),
  ADD KEY `mails_mail_from_foreign` (`mail_from`),
  ADD KEY `mails_mail_to_foreign` (`mail_to`);

--
-- Indexes for table `excel_migrations`
--
ALTER TABLE `excel_migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `excel_password_resets`
--
ALTER TABLE `excel_password_resets`
  ADD KEY `password_resets_username_index` (`username`);

--
-- Indexes for table `excel_types`
--
ALTER TABLE `excel_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `excel_users`
--
ALTER TABLE `excel_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_username_unique` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `excel_dispositions`
--
ALTER TABLE `excel_dispositions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `excel_logsurat`
--
ALTER TABLE `excel_logsurat`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `excel_mails`
--
ALTER TABLE `excel_mails`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `excel_migrations`
--
ALTER TABLE `excel_migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=107;

--
-- AUTO_INCREMENT for table `excel_types`
--
ALTER TABLE `excel_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `excel_users`
--
ALTER TABLE `excel_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `excel_dispositions`
--
ALTER TABLE `excel_dispositions`
  ADD CONSTRAINT `dispositions_id_mail_foreign` FOREIGN KEY (`id_mail`) REFERENCES `excel_mails` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `dispositions_mail_to_foreign` FOREIGN KEY (`mail_to`) REFERENCES `excel_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `excel_mails`
--
ALTER TABLE `excel_mails`
  ADD CONSTRAINT `mails_id_type_foreign` FOREIGN KEY (`id_type`) REFERENCES `excel_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `mails_mail_from_foreign` FOREIGN KEY (`mail_from`) REFERENCES `excel_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `mails_mail_to_foreign` FOREIGN KEY (`mail_to`) REFERENCES `excel_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
