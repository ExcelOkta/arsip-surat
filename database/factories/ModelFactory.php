<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;
    return [
        'name' => $faker->name,
        'username' => $faker->unique()->userName,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
        'level' => 'Guru',
        // 'level' => $faker->shuffle(array('Kesiswaan', '', 'Kesiswaan', 'Kurikulum')),
        'photo' => 'profile.jpg',
    ];
});

$factory->define(App\Type::class, function (Faker\Generator $faker) {
    return [
      'type' => $faker->word,
    ];
});

$factory->define(App\Mail::class, function (Faker\Generator $faker) {
    return [
      'mail_code' => date("mdHis")."/SIAS/".date("Y"),
      'mail_from' => 1,
      'mail_to' => App\User::all()->random()->id,
      'mail_subject' => $faker->text($maxNbChars = 80),
      'description' => $faker->paragraph,
      'file' => 'null',
      'id_type' => App\Type::all()->random()->id,
      'mark' => "unread"
    ];
});

$factory->state(App\Mail::class, 'masuk', function (Faker\Generator $faker) {
    return [
      'mail_code' => date("mdHis")."/SIAS/".date("Y"),
      'mail_from' => App\User::all()->random()->id,
      'mail_to' => 1,
      'mail_subject' => $faker->text($maxNbChars = 80),
      'description' => $faker->paragraph,
      'file' => 'null',
      'id_type' => App\Type::all()->random()->id,
      'mark' => "unread"
    ];
});

$factory->define(App\Disposition::class, function (Faker\Generator $faker) {
    return [
      'id_mail' => App\Mail::all()->random()->id,
      'mail_from' => 1,
      'mail_to' => App\User::all()->random()->id,
      'description' => $faker->paragraph,
      'mark' => "unread"
    ];
});

$factory->state(App\Disposition::class, 'masuk', function (Faker\Generator $faker) {
    return [
        'id_mail' => App\Mail::all()->random()->id,
        'mail_from' => App\User::all()->random()->id,
        'mail_to' => 1,
        'description' => $faker->paragraph,
        'mark' => "unread"
    ];
});
