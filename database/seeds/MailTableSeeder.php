<?php

use Illuminate\Database\Seeder;

class MailTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $mails = factory(\App\Mail::class, 100)->create();
        $mails = factory(\App\Mail::class, 50)->states('masuk')->create();
    }
}
