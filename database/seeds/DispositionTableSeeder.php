<?php

use Illuminate\Database\Seeder;

class DispositionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $dispositions = factory(\App\Disposition::class, 100)->create();
      $dispositions = factory(\App\Disposition::class, 50)->states('masuk')->create();
    }
}
