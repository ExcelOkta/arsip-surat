<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogTrigger extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared('
        CREATE TRIGGER tr_logsurat AFTER INSERT ON `excel_mails` FOR EACH ROW
          BEGIN
            INSERT INTO excel_logsurat (`id`, `mail_subject`, `mail_from`, `mail_to`, `created_at`, `updated_at`)
            VALUES (null, NEW.mail_subject, NEW.mail_from, NEW.mail_to, now(), null);
          END
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP TRIGGER `tr_logsurat`');
    }
}
