<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mail extends Model
{
  protected $fillable = [
      'mail_code', 'from', 'mail_from', 'mail_to', 'mail_subject', 'description', 'file', 'id_type', 'mark', 'status'
  ];
}
