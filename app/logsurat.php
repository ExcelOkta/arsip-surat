<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class logsurat extends Model
{

  protected $table = "logsurat";

  protected $fillable = [
      'mail_from', 'mail_to', 'mail_subject'
  ];
}
