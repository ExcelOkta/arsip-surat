<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
use Illuminate\Support\Facades\Validator;

class AnggotaController extends Controller
{

    public function index()
    {
      $data['users'] = User::orderBy('created_at', 'desc')->paginate(5);
      return view('dashboard.anggota',$data);
    }

    public function cari(Request $request)
    {
      $data['users'] = User::where('name', 'like', '%'.$request->input('cari').'%')
      ->orWhere('username', 'like', '%'.$request->input('cari').'%')
      ->paginate(10);
      return view('dashboard.anggota',$data);
    }

    public function edit() {
      $data['user'] = User::where('id', Auth::id())->first();
      return view('dashboard.profile', $data);
    }

    public function store(Request $request)
    {
      User::create([
          'name' => $request->input('name'),
          'username' => $request->input('username'),
          'password' => bcrypt($request->input('password')),
          'level' => $request->input('level'),
          'photo' => "profile.jpg"
      ]);
      return redirect()->back();
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->file(), array('files' => 'max:1000|mimes:jpeg,png,jpg'));
        if ($validator->fails()) {
          return redirect()->back()->withErrors($validator);
        } else {
          if ($request->file('files')) {
            $file = $request->file('files');
            $filename = time()."_".$file->getClientOriginalName();
            $file->move(public_path('/files'), $filename);
          } else {
            $filename = 'null';
          }
          $type = User::find($id);
          $type->name = $request->get('name');
          if (!empty($request->get('username'))) {
            $type->username = $request->get('username');
          }
          if (!empty($request->get('password'))) {
            $type->password = bcrypt($request->get('password'));
          }
          if (!empty($request->get('level'))) {
            $type->level = $request->get('level');
          }
          if (!empty($request->file('files'))) {
            $type->photo = $filename;
          }
          $type->save();
          return redirect(route('anggota'));
        }
    }

    public function destroy($id)
    {
        $type = User::find($id)->delete();
        return redirect()->back();
    }
}
