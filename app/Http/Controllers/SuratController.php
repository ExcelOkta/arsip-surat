<?php

namespace App\Http\Controllers;

use Auth;
use App\Mail;
use App\Type;
use App\User;
use App\Disposition;
use App\logsurat;
use PDF;
use DB;
use Carbon\Carbon;
use Illuminate\Http\Request;

class SuratController extends Controller
{

    public function index()
    {
      $data['masuk'] = Mail::where('mail_to', Auth::id())->count();
      $data['keluar'] = Mail::where('mail_from', Auth::id())->count();
      $data['disposisi_masuk'] = Disposition::where('mail_to', Auth::id())->count();
      $data['disposisi_keluar'] = Disposition::where('mail_from', Auth::id())->count();
      $data['unread_surat'] = Mail::where(['mail_to' => Auth::id(), 'mark' => "unread"])->count();
      $data['unread_disposisi'] = Disposition::where(['mail_to' => Auth::id(), 'mark' => "unread"])->count();
      $data['log'] = logsurat::orderBy('created_at', 'desc')->get();
      return view('dashboard.index',$data);
    }

    public function masuk()
    {
      $data['surat'] = Mail::where('status', 'Surat')
      ->Where('mail_from', Auth::user()->id)
      ->orWhere('mail_to', Auth::user()->id)
      ->orderBy('created_at', 'desc')->paginate(5);
      $data['title'] = "Surat Masuk";
      $data['dis'] = false;
      return view('dashboard.masuk', $data);
    }

    public function keluar()
    {
      $data['surat'] = Mail::where('status', 'Arsip')->orderBy('created_at', 'desc')->paginate(5);
      $data['title'] = "Surat Keluar";
      $data['dis'] = false;
      return view('dashboard.keluar', $data);
    }

    public function arsip($id)
    {
      $mail = Disposition::find($id);
      $mail->status = "Arsip";
      $mail->save();
      return redirect('disposisi/masuk');
    }

    public function lanjut($id)
    {
      $surat = DB::table('dispositions')
      ->join('mails', 'dispositions.id_mail', '=', 'mails.id')
      ->select('dispositions.*', 'dispositions.description AS pesan', 'mails.from', 'mails.mail_from', 'mails.mail_to', 'mails.mail_subject', 'mails.id_type', 'mails.description', 'mails.mail_code', 'mails.file')
      ->where('dispositions.id', $id)
      ->first();
      Mail::create([
        'mail_code' => date("mdHis").rand(100,999)."/SIAS/".date("Y"),
        'from' => $surat->from,
        'mail_from' => $surat->mail_from,
        'mail_to' => User::where('level', 'Kepala Sekolah')->value('id'),
        'mail_subject' => $surat->mail_subject,
        'description' => $surat->description,
        'file' => $surat->file,
        'status' => 'Arsip',
        'id_type' => $surat->id_type,
        'mark' => "unread"
      ]);
      return redirect('surat/keluar');
    }

    public function baca($id)
    {
      $data['surat'] = Mail::where('id', $id)->first();
      $data['dis'] = false;
      if ($data['surat']->mail_from != Auth::user()->id) {
        if ($data['surat']->mark == "unread") {
          $edit = Mail::find($id);
          $edit->mark = "read";
          $edit->save();
        }
      }
      $data['users'] = User::all();
      return view('dashboard.baca',$data);
    }

    public function cari(Request $request)
    {
      $data['title'] = "Cari: ".$request->input('cari');
      $data['dis'] = false;
      if ($request->input('type') == 'masuk') {
        $data['surat'] = Mail::where('mail_to', Auth::user()->id)
        ->where('mail_subject', 'like', '%'.$request->input('cari').'%')
        ->orderBy('created_at', 'desc')
        ->paginate(10);
        return view('dashboard.masuk',$data);
      } else if($request->input('type') == 'keluar') {
        $data['surat'] = Mail::where('mail_from', Auth::user()->id)
        ->where('mail_subject', 'like', '%'.$request->input('cari').'%')
        ->orderBy('created_at', 'desc')
        ->paginate(10);
        return view('dashboard.keluar',$data);
      } else {
        echo "ea lohhh..";
      }
    }

    public function laporan()
    {
      $data['title'] = "Laporan";
      return view('dashboard.laporan',$data);
    }

    public function laporan_post(Request $request)
    {
      if ($request->input('tipe') == "disposisi") {
        $data['dis'] = true;
        if ($request->input('jenis') == "masuk") {
          $data['surat'] =  DB::table('dispositions')
          ->join('mails', 'dispositions.id_mail', '=', 'mails.id')
          ->select('dispositions.*', 'mails.mail_subject', 'mails.from', 'mails.file', 'mails.mail_code', 'mails.id_type')
          ->where('dispositions.mail_from', Auth::id())
          ->orWhere('dispositions.mail_to', Auth::id())
          ->where('dispositions.created_at', '>=', $request->input('dari'))
          ->where('dispositions.created_at', '<=', $request->input('sampai'))
          ->orderBy('dispositions.created_at', 'desc');
        }
        if ($request->input('jenis') == "keluar") {
          $data['surat'] = DB::table('dispositions')
          ->join('mails', 'dispositions.id_mail', '=', 'mails.id')
          ->select('dispositions.*', 'mails.mail_subject', 'mails.from', 'mails.file', 'mails.mail_code', 'mails.id_type')
          ->where('dispositions.mail_from', Auth::id())
          ->orWhere('dispositions.mail_to', Auth::id())
          ->where('dispositions.created_at', '>=', $request->input('dari'))
          ->where('dispositions.created_at', '<=', $request->input('sampai'))
          ->orderBy('dispositions.created_at', 'desc');
        }
      } else if ($request->input('tipe') == "surat") {
        $data['dis'] = false;
        if ($request->input('jenis') == "masuk") {
          $data['surat'] = Mail::where('status', 'Surat')
          ->Where('mail_from', Auth::user()->id)
          ->orWhere('mail_to', Auth::user()->id)
          ->where('created_at', '>=', $request->input('dari'))
          ->where('created_at', '<=', $request->input('sampai'))
          ->orderBy('created_at', 'desc');
        }
        if ($request->input('jenis') == "keluar") {
          $data['surat'] = Mail::where('status', 'Arsip')
          ->Where('mail_from', Auth::user()->id)
          ->orWhere('mail_to', Auth::user()->id)
          ->where('created_at', '>=', $request->input('dari'))
          ->where('created_at', '<=', $request->input('sampai'))
          ->orderBy('created_at', 'desc');
        }
      }
      $data['req'] = $request;
      if ($request->input('button') == "Unduh") {
        $pdf = PDF::loadView('dashboard.print',$data);
        return $pdf->download('laporan_sias_'.date("mdHis").'.pdf');
      } else {
        $pdf = PDF::loadView('dashboard.print',$data);
        return $pdf->stream('print.pdf');
      }
    }

    public function create()
    {
      $data['tipe'] = Type::all();
      $data['users'] = User::all();
      return view('dashboard.kirim',$data);
    }

    public function store(Request $request)
    {
      if ($request->file('files')) {
        $file = $request->file('files');
        $filename = time()."_".$file->getClientOriginalName();
        $file->move(public_path('/files'), $filename);
      } else {
        $filename = 'null';
      }
      Mail::create([
        'mail_code' => $request->input('nomor'),
        'from' => $request->input('from'),
        'mail_from' => Auth::user()->id,
        'mail_to' => User::where('level', 'Kepala Sekolah')->value('id'),
        'mail_subject' => $request->input('subjek'),
        'description' => $request->input('isi'),
        'file' => $filename,
        'status' => 'Surat',
        'id_type' => $request->input('tipe'),
        'mark' => "unread"
      ]);
      return redirect('surat/masuk');
    }

    public function destroy($id)
    {
        $mail = Mail::find($id)->delete();
        return redirect()->back();
    }
}
