<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail;
use App\Disposition;
use Auth;
use App\Type;
use App\User;
use DB;

class DisposisiController extends Controller
{
  public function masuk()
  {
    $data['surat'] = DB::table('dispositions')
    ->join('mails', 'dispositions.id_mail', '=', 'mails.id')
    ->select('dispositions.*', 'mails.mail_subject', 'mails.from', 'mails.file', 'mails.mail_code', 'mails.id_type')
    ->where('dispositions.mail_from', Auth::id())
    ->orWhere('dispositions.mail_to', Auth::id())
    ->orderBy('created_at', 'desc')
    ->paginate(10);
    $data['title'] = "Disposisi";
    $data['dis'] = true;
    return view('dashboard.masuk', $data);
  }

  public function baca($id)
  {
    $data['surat'] = DB::table('dispositions')
    ->join('mails', 'dispositions.id_mail', '=', 'mails.id')
    ->select('dispositions.*', 'dispositions.description AS pesan', 'mails.from', 'mails.mail_subject', 'mails.id_type', 'mails.description', 'mails.mail_code', 'mails.file')
    ->where('dispositions.id', $id)
    ->first();
    if ($data['surat']->mail_from != Auth::user()->id) {
      if ($data['surat']->mark == "unread") {
        $edit = Disposition::find($id);
        $edit->mark = "read";
        $edit->save();
      }
    }
    $data['dis'] = true;
    return view('dashboard.baca',$data);
  }

  public function cari(Request $request)
  {
    $data['title'] = "Cari: ".$request->input('cari');
    $data['dis'] = true;
    if ($request->input('type') == 'masuk') {
      $data['surat'] = DB::table('dispositions')
      ->join('mails', 'dispositions.id_mail', '=', 'mails.id')
      ->select('dispositions.*', 'mails.mail_subject', 'mails.id_type')
      ->where('dispositions.mail_to', Auth::id())
      ->where('mail_subject', 'like', '%'.$request->input('cari').'%')
      ->orderBy('created_at', 'desc')
      ->paginate(10);
      return view('dashboard.masuk',$data);
    } else if($request->input('type') == 'keluar') {
      $data['surat'] = DB::table('dispositions')
      ->join('mails', 'dispositions.id_mail', '=', 'mails.id')
      ->select('dispositions.*', 'mails.mail_subject', 'mails.id_type')
      ->where('dispositions.mail_from', Auth::id())
      ->where('mail_subject', 'like', '%'.$request->input('cari').'%')
      ->orderBy('created_at', 'desc')
      ->paginate(10);
      return view('dashboard.keluar',$data);
    } else {
      echo "ea lohhh..";
    }
  }

  public function store(Request $request)
  {
    Disposition::create([
      'id_mail' => $request->input('id_mail'),
      'mail_from' => Auth::user()->id,
      'mail_to' => $request->input('user'),
      'description' => $request->input('pesan'),
      'mark' => "unread",
      'status' => "Surat"
    ]);
    return redirect('disposisi/masuk');
  }

  public function destroy($id)
  {
    $mail = Disposition::find($id);
    $mail->delete();
    return redirect('/disposisi/masuk');
  }
}
