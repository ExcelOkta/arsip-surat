<?php

namespace App\Http\Controllers;

use App\Type;
use Illuminate\Http\Request;

class TipeController extends Controller
{
    public function index()
    {
      $data['tipe'] = Type::orderBy('created_at', 'desc')->paginate(5);
      return view('dashboard.tipe',$data);
    }

    public function cari(Request $request)
    {
        $data['tipe'] = Type::where('type', 'like', '%'.$request->input('cari').'%')
        ->orderBy('created_at', 'desc')
        ->paginate(10);
        return view('dashboard.tipe',$data);
    }

    public function store(Request $request)
    {
        Type::create([
            'type'  =>  $request->input('jenis'),
        ]);
        return redirect()->back();
    }

    public function update(Request $request, $id)
    {
        $type = Type::find($id);
        $type->type = $request->get('jenis');
        $type->save();
        return redirect()->back();
    }

    public function destroy($id)
    {
        $type = Type::find($id);
        $type->delete();
        return redirect()->back();
    }
}
