@extends('layouts.dashboard')
@section('title', $title)

@section('content')
<h3 class="page-heading mb-4"><i class="fa fa-print"></i> @yield('title')</h3>
<div class="card-deck">
  <div class="card col-lg-12 px-0 mb-4">
    <div class="card-body">
      <form action="{{ route('post_laporan') }}" method="post" target="_blank">
        {{ csrf_field() }}
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label>Dari Tanggal</label>
              <input type="date" name="dari" class="form-control" required>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label>Sampai Tanggal</label>
              <input type="date" name="sampai" class="form-control" required>
            </div>
          </div>
        </div>
        <label>Pilih Jenis:</label>
        <div class="form-group">
          <div class="form-radio">
            <label>
              <input name="tipe" value="disposisi" type="radio" required>
              Disposisi
            <i class="input-helper"></i></label>
          </div>
        <div class="form-group">
          <div class="form-radio">
            <label>
              <input name="tipe" value="surat" type="radio" required>
              Surat
            <i class="input-helper"></i></label>
          </div>
        <br>
        <label>Pilih Jenis:</label>
        <div class="form-group">
          <div class="form-radio">
            <label>
              <input name="jenis" value="masuk" type="radio" required>
              Masuk
            <i class="input-helper"></i></label>
          </div>
        </div>
        <div class="form-group">
          <div class="form-radio">
            <label>
              <input name="jenis" value="keluar" type="radio" required>
              Keluar
            <i class="input-helper"></i></label>
          </div>
        </div>
        <br>
        <div class="row">
          <div class="col-md-6">
            <input type="submit" name="button" class="btn btn-outline-primary btn-block" value="Tampilkan">
          </div>
          <div class="col-md-6">
            <input type="submit" name="button" class="btn btn-outline-primary btn-block" value="Unduh">
          </div>
        </div>
      </form>
    </div>
  </div>
</div>

@endsection
