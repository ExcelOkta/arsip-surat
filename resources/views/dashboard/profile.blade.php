@extends('layouts.dashboard')
@section('title', 'Kirim Surat')
@section('content')
<h3 class="page-heading mb-4">Profile</h3>
<div class="card-deck">
  <div class="card col-lg-12 px-0 mb-4">
    <div class="card-body">
      <form method="post" action="{{ route('update_anggota', Auth::id()) }}" enctype="multipart/form-data">
        {{ method_field('PUT') }}
        {{ csrf_field() }}
        <div class="form-group">
          <label>Nama</label>
          <input type="text" class="form-control" name="name" value="{{ $user->name }}">
        </div>
        <div class="form-group">
          <label>Username</label>
          <input type="text" class="form-control" name="username" value="{{ $user->username }}" disabled>
        </div>
        <div class="form-group">
          <label>Password</label>
          <input type="text" class="form-control" name="password">
          <small>Kosongkan jika tidak ingin mengganti password.</small>
        </div>
        <div class="row">
          <div class="col-sm-6">
            <div class="form-group">
              <label>Level</label>
              <input type="text" class="form-control" name="level" value="{{ $user->level }}" disabled>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-group">
              <label for="exampleInputFile">Foto Profile</label>
              <input type="file" class="form-control" name="files">
            </div>
          </div>
        </div>
        <button type="submit" class="btn btn-outline-primary btn-block">Kirim</button>
      </form>
    </div>
  </div>
</div>
@endsection
