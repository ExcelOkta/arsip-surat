<title>SIAS - Laporan</title>
<center>
  <h2 style="margin:0;padding:0;">SEKOLAH MENENGAH KEJURUAN</h2>
  <h4 style="margin:0;padding:0;">LAPORAN APLIKASI SURAT MENYURAT</h4>
</center>
<hr/>
<pre>
Pembuat Laporan : {{ Auth::user()->name }}
Rentang Waktu   : {{ $req->get('dari') }} - {{ $req->get('sampai') }}
Jenis           : {{$req->get('tipe')}} {{$req->get('jenis')}}
Jumlah          : {{ $surat->count() }} {{$req->get('tipe')}}
</pre>
<hr/>
<table border="1" width="100%">
  <thead>
    <tr align="center">
      <th>Tipe</th>
      <th>Kode Surat</th>
      <th>Judul</th>
      <th>Tanggal</th>
      <th>Pengirim</th>
    </tr>
  </thead>
  <tbody>
    @foreach($surat->get() as $mail)
    <tr align="center">
      <td>{{ App\Type::where('id', $mail->id_type)->value('type') }}</td>
      <td>{{ $mail->mail_code }}</td>
      <td>{{ $mail->mail_subject }}</td>
      <td>{{ $mail->created_at }}</td>
      <td>{{ App\User::where('id', $mail->mail_from)->value('name') }}</td>
    </tr>
    @endforeach
  </tbody>
</table>
