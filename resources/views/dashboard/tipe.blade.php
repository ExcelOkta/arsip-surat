@extends('layouts.dashboard')
@section('title', 'Entri Data Jenis Surat')

@section('content')

<div class="card-deck">
  <div class="card col-lg-12 px-0 mb-4">
    <div class="card-header">Tambah Jenis Surat</div>
    <div class="card-body">
      <form action="{{ route('post_jenis') }}" method="post">
        {!! csrf_field() !!}
        <div class="form-group">
          <label>Jenis Surat</label>
          <input type="text" class="form-control" name="jenis">
        </div>
        <button type="submit" name="button" class="btn btn-outline-primary btn-block">Tambah</button>
      </form>
    </div>
  </div>
</div>
<div class="card-deck">
  <div class="card col-lg-12 px-0 mb-4">
    <div class="card-body">
      <div class="card-title">
        <form action="{{ route('cari_jenis') }}" method="post">
          {!! csrf_field() !!}
          <input type="text" class="form-control" placeholder="Pencarian" name="cari">
        </form>
      </div>
    </div>

    <div class="table-responsive">
      <table class="table table-inverse table-striped">
        <thead>
          <tr>
            <th>Jenis</th>
            <th></th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          @foreach($tipe as $type)
          <tr class="">
            <td>{{ $type->type }}</td>
            <td width="100">
              <button class="btn btn-outline-success btn-sm" data-toggle="modal" data-target="#jenis{{$type->id}}"><i class="fa fa-pencil"></i> Sunting</button>
              <div class="modal fade" id="jenis{{$type->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title">Sunting: {{ $type->type }}</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                      <form action="{{ route('update_jenis', $type->id) }}" method="post">
                    <div class="modal-body">
                        {{ method_field('PUT') }}
                        {{ csrf_field() }}
                        <div class="form-group">
                          <label>Jenis Surat</label>
                          <input type="text" class="form-control" name="jenis" value="{{$type->type}}">
                        </div>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                      <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                  </form>
                  </div>
                </div>
              </div>
            </td>
            <td><a href="{{ route('delete_jenis', $type->id) }}" class="btn btn-outline-danger btn-sm"><i class="fa fa-trash"></i> Hapus</a></td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
    {{ $tipe->links() }}
  </div>
</div>
@endsection
