@extends('layouts.dashboard')
@section('title', 'Kirim Surat')
@section('content')
<h3 class="page-heading mb-4">Buat Surat</h3>
<div class="card-deck">
  <div class="card col-lg-12 px-0 mb-4">
    <div class="card-body">
      <form method="post" action="{{ route('post_mail') }}" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="row">
          <div class="col-md-6">
          <div class="form-group">
            <label for="exampleInputEmail1">Nomor Surat</label>
            <input type="text" class="form-control" name="nomor">
          </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label for="exampleInputEmail1">Surat Dari</label>
              <input type="text" class="form-control" name="from">
            </div>
          </div>
        </div>
        <div class="form-group">
          <label for="exampleInputEmail1">Subjek Surat</label>
          <input type="text" class="form-control" name="subjek">
        </div>
        <div class="form-group">
          <label for="exampleInputEmail1">Pesan</label>
          <textarea class="form-control" rows="10" name="isi"></textarea>
        </div>
        <div class="row">
          <div class="col-sm-6">
            <div class="form-group">
              <label for="exampleInputEmail1">Tipe Surat</label>
              <select class="form-control" name="tipe">
                @foreach($tipe as $type)
                <option value="{{ $type->id }}">{{ $type->type }}</option>
                @endforeach
              </select>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-group">
              <label for="exampleInputFile">Upload Berkas</label>
              <input type="file" class="form-control" name="files">
            </div>
          </div>
        </div>
        <button type="submit" class="btn btn-outline-primary btn-block">Kirim</button>
      </form>
    </div>
  </div>
</div>
@endsection
