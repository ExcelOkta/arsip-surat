@extends('layouts.dashboard')
@section('title', $title)

@section('content')
<h3 class="page-heading mb-4"><i class="fa fa-download"></i> @yield('title')</h3>
<div class="card-deck">
  <div class="card col-lg-12 px-0 mb-4">
    <div class="card-body">

        <div class="card-title">
          <div class="row">
            @if(Auth::user()->level == "Tata Usaha")
            <div class="col-md-10">
            @else
            <div class="col-md-12">
            @endif
              @if($dis)
              <form action="{{ route('cari_disposisi') }}" method="post">
              @else
              <form action="{{ route('cari_surat') }}" method="post">
              @endif
                {!! csrf_field() !!}
                <input type="text" class="form-control" placeholder="Pencarian" name="cari">
                <input type="hidden" name="type" value="masuk">
              </form>
            </div>
            <div class="col-md-2">
              @if(Auth::user()->level == "Tata Usaha")
              <a href="{{ route('kirim_surat')}}" class="btn btn-block btn-outline-primary"><i class="fa fa-plus"></i> Buat Surat</a>
              @endif
            </div>
          </div>
        </div>
    </div>

    <div class="table-responsive">
      <table class="table table-inverse">
        <thead>
          <tr>
            <th>Tipe</th>
            <th>Judul</th>
            <th>Tanggal</th>
            <th>Nomor Surat</th>
            <th></th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          @foreach($surat as $mail)
          <tr @if($mail->mark == "unread") style="background-color:#202f3c;" @endif>
            <td><label class="badge badge-success">{{ App\Type::where('id', $mail->id_type)->value('type') }}</label></td>
            <td>{{ $mail->mail_subject }}</td>
            <td>{{ $mail->created_at }}</td>
            <td>{{ $mail->mail_code }}</td>
            @if($mail->file != "null")
            <td width="100">
              @if($dis)
                @if($mail->status == "Arsip")
                <a href="" class="btn btn-outline-default btn-sm"><i class="fa fa-download"></i> Unduh</a>
                @else
                <a href="{{ route('disposisi_baca',$mail->id) }}" class="btn btn-outline-success btn-sm"><i class="fa fa-eye"></i> Lanjutkan</a>
                @endif
              @else
              <a href="{{ url('surat/baca/'.$mail->id) }}" class="btn btn-outline-success btn-sm"><i class="fa fa-eye"></i> Baca</a>
              @endif
            </td>
            <td width="100">
              @if($dis)
                @if($mail->status == "Arsip")
                <button type="button" class="btn btn-outline-warning btn-sm" disabled><i class="fa fa-file"></i> Arsip</button>
                @else
                <a href="{{ route('arsip',$mail->id) }}" class="btn btn-outline-warning btn-sm"><i class="fa fa-file"></i> Arsip</a>
                @endif
              @else
              <a href="{{ url('surat/hapus/'.$mail->id) }}" class="btn btn-outline-danger btn-sm"><i class="fa fa-trash"></i> Hapus</a>
              @endif
            </td>
            @endif
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
    {{ $surat->links() }}
  </div>
</div>
@endsection
