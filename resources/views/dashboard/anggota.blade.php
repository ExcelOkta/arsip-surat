@extends('layouts.dashboard')
@section('title', 'Entri Data Anggota')

@section('content')

<div class="card-deck">
  <div class="card col-lg-12 px-0 mb-4">
    <div class="card-header">Tambah Anggota</div>
    <div class="card-body">
      <form action="{{ route('post_anggota') }}" method="post">
        {!! csrf_field() !!}
        <div class="form-group">
          <label>Nama</label>
          <input type="text" class="form-control" name="name">
        </div>
        <div class="form-group">
          <label>Username</label>
          <input type="text" class="form-control" name="username">
        </div>
        <div class="form-group">
          <label>Password</label>
          <input type="password" class="form-control" name="password">
        </div>
        <div class="form-group">
          <label>Level</label>
          <select class="form-control" name="level">
            <option value="Guru">Guru</option>
            <option value="Kepala Sekolah">Kepala Sekolah</option>
            <option value="Tata Usaha">Tata Usaha</option>
            <option value="Kurikulum">Kurikulum</option>
            <option value="Kesiswaan">Kesiswaan</option>
          </select>
        </div>
        <button type="submit" name="button" class="btn btn-outline-primary btn-block">Tambah</button>
      </form>
    </div>
  </div>
</div>
<div class="card-deck">
  <div class="card col-lg-12 px-0 mb-4">
    <div class="card-body">
      <div class="card-title">
        <form action="{{ route('cari_anggota') }}" method="post">
          {!! csrf_field() !!}
          <input type="text" class="form-control" placeholder="Pencarian" name="cari">
        </form>
      </div>
    </div>

    <div class="table-responsive">
      <table class="table table-inverse table-striped">
        <thead>
          <tr>
            <th>Nama</th>
            <th>Username</th>
            <th>Level</th>
            <th>Tanggal Bergabung</th>
            <th></th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          @foreach($users as $user)
          <tr class="">
            <td>{{ $user->name }}</td>
            <td>{{ $user->username }}</td>
            <td>{{ $user->level }}</td>
            <td>{{ $user->created_at }}</td>
            <td width="100">
              <button class="btn btn-outline-success btn-sm" data-toggle="modal" data-target="#jenis{{$user->id}}"><i class="fa fa-pencil"></i> Sunting</button>
              <div class="modal fade" id="jenis{{$user->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title">Sunting: {{ $user->type }}</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                      <form action="{{ route('update_anggota', $user->id) }}" method="post">
                    <div class="modal-body">
                        {{ method_field('PUT') }}
                        {{ csrf_field() }}
                        <div class="form-group">
                          <label>Nama</label>
                          <input type="text" class="form-control" name="name" value="{{ $user->name }}">
                        </div>
                        <div class="form-group">
                          <label>Username</label>
                          <input type="text" class="form-control" name="username" value="{{ $user->username }}">
                        </div>
                        <div class="form-group">
                          <label>Password</label>
                          <input type="password" class="form-control" name="password">
                        </div>
                        <div class="form-group">
                          <label>Level</label>
                          <select class="form-control" name="level">
                            <option value="{{ $user->level }}">{{ $user->level }}</option>
                            <option value="Guru">Guru</option>
                            <option value="Kepala Sekolah">Kepala Sekolah</option>
                            <option value="Tata Usaha">Tata Usaha</option>
                            <option value="Kurikulum">Kurikulum</option>
                            <option value="Kesiswaan">Kesiswaan</option>
                          </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                      <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                  </form>
                  </div>
                </div>
              </div>
            </td>
            <td><a href="{{ route('delete_anggota', $user->id) }}" class="btn btn-outline-danger btn-sm"><i class="fa fa-trash"></i> Hapus</a></td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
    {{ $users->links() }}
  </div>
</div>
@endsection
