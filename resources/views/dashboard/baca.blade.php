@extends('layouts.dashboard')
@section('title', $surat->mail_subject)

@section('content')
<div class="card-deck">
  <div class="card col-lg-12 px-0 mb-4">
    <div class="card-body">
      <h5 class="card-title">{{$surat->mail_subject}}</small> </h5>
      <h6 class="card-subtitle mb-2 text-muted"><span class="badge badge-success"><i class="fa fa-user"></i> {{ App\User::where('id', $surat->mail_from)->value('level') }}</span> {{ App\User::where('id', $surat->mail_from)->value('name') }} | <i class="fa fa-clock-o"></i> {{$surat->created_at}}</h6>
      <hr>
      <div class="row">
        <div class="col-md-6">
          <label><b>Kode Surat:</b></label>
          <p>{{ $surat->mail_code }}</p>
        </div>
        <div class="col-md-6">
          <label><b>Pengirim:</b></label>
          <p>{{ $surat->from }}</p>
        </div>
        <div class="col-md-6">
          <label><b>Tipe Surat:</b></label>
          <p>{{ App\Type::where('id', $surat->id_type)->value('type') }}</p>
        </div>
        <div class="col-md-6">
          <label><b>Penerima:</b></label>
          <p>{{ App\User::where('id', $surat->mail_to)->value('name') }}</p>
        </div>
      </div>
      <hr>
      @if($dis)
      <label><b>Perihal Disposisi:</b></label>
      <p>{!! $surat->pesan !!}</p>
      <hr>
      @endif
      <p class="card-text">{!! $surat->description !!}</p>
      @if($surat->file != "null")
      <div class="bg-dark">
        <div class="card-body text-center">
          <h5 class="text-primary"><i class="fa fa-file"></i> {{ $surat->file }}</h5>
          <br>
          <a href="{{ url('/files/'.$surat->file) }}" class="btn btn-outline-primary"><i class="fa fa-download"></i> Unduh</a>
        </div>
      </div>
      @endif
      <hr>
      @if(Auth::user()->level == "Kepala Sekolah")
        @if(!$dis)
        <div class="collapse" id="collapseExample">
          <div class="card card-body">
            <form method="post" action="{{ route('disposisi_post') }}">
              {{ csrf_field() }}
              <div class="form-group">
                <label for="exampleInputEmail1">Penerima</label>
                <select class="form-control" size="5" name="user">
                  @foreach($users as $user)
                  <option value="{{$user->id}}">{{$user->name}} ({{$user->level}})</option>
                  @endforeach
                </select>
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Pesan</label>
                <select class="form-control" name="pesan">
                  <option value="Arsipkan">Arsipkan</option>
                  <option value="Lanjutkan">Lanjutkan</option>
                </select>
              </div>
              <input type="hidden" value="{{$surat->id}}" name="id_mail">
              <button type="submit" class="btn btn-outline-primary btn-block">Kirim</button>
            </form>
          </div>
        </div>
        <br>
        @endif
      @endif

        @if(!$dis)
        <div class="row">
          <div class="col-md-6">
            <a href="{{ url()->previous() }}" class="btn btn-outline-success btn-block"><i class="fa fa-chevron-left"></i> Kembali</a>
          </div>
          @if(Auth::user()->level == "Kepala Sekolah")
          <div class="col-md-6">
            <button class="btn btn-outline-primary btn-block" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample"><i class="fa fa-forward"></i> Disposisi</button>
          </div>
          @endif
        </div>
        @else
        <div class="row">
          <div class="col-md-12">
            <a href="{{ route('lanjut', $surat->id) }}" class="btn btn-outline-success btn-block"><i class="fa fa-bookmark"></i> Tambah Surat Keluar</a>
          </div>
        </div>
        @endif
    </div>

  </div>
</div>
@endsection
