@extends('layouts.dashboard')
@section('title', 'Dashboard')

@section('content')
<h3 class="page-heading mb-4">Dashboard</h3>
<div class="card">
  <div class="card-body">
    <h5>Hallo, {{ Auth::user()->name }}</h5>
    <p>Kami ingin memberikan pemberitahuan bahwa:</p>
    <p><a href="{{ route('surat_masuk') }}">{{ $unread_surat }} Surat Belum Dibaca</a></p>
    <p><a href="{{ route('disposisi_masuk') }}">{{ $unread_disposisi }} Disposisi Belum Dibaca</a></p>
  </div>
</div>
<br>
<div class="row">
  <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 mb-4">
    <div class="card">
      <div class="card-body">
        <h5 class="card-title mb-4">Chart Surat Masuk</h5>
        <canvas id="SuratMasuk" class="text-white" style="height:230px"></canvas>
      </div>
    </div>
  </div>
  <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 mb-4">
    <div class="card">
      <div class="card-body">
        <h5 class="card-title mb-4">Chart Surat Keluar</h5>
        <canvas id="SuratKeluar" class="text-white" style="height:230px"></canvas>
      </div>
    </div>
  </div>
  <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 mb-4">
    <div class="card card-statistics">
      <div class="card-body">
        <div class="clearfix">
          <div class="float-left">
            <h4 class="text-info">
              <i class="fa fa-download highlight-icon" aria-hidden="true"></i>
            </h4>
          </div>
          <div class="float-right">
            <p class="card-text">Surat Masuk</p>
            <h4 class="bold-text">{{ $masuk }}</h4>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 mb-4">
    <div class="card card-statistics">
      <div class="card-body">
        <div class="clearfix">
          <div class="float-left">
            <h4 class="text-info">
              <i class="fa fa-upload highlight-icon" aria-hidden="true"></i>
            </h4>
          </div>
          <div class="float-right">
            <p class="card-text">Surat Keluar</p>
            <h4 class="bold-text">{{ $keluar }}</h4>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 mb-4">
    <div class="card card-statistics">
      <div class="card-body">
        <div class="clearfix">
          <div class="float-left">
            <h4 class="text-warning">
              <i class="fa fa-download highlight-icon" aria-hidden="true"></i>
            </h4>
          </div>
          <div class="float-right">
            <p class="card-text">Disposisi Masuk</p>
            <h4 class="bold-text">{{ $disposisi_keluar }}</h4>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 mb-4">
    <div class="card card-statistics">
      <div class="card-body">
        <div class="clearfix">
          <div class="float-left">
            <h4 class="text-warning">
              <i class="fa fa-upload highlight-icon" aria-hidden="true"></i>
            </h4>
          </div>
          <div class="float-right">
            <p class="card-text">Disposisi Keluar</p>
            <h4 class="bold-text">{{ $disposisi_keluar }}</h4>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@if(Auth::user()->level == "Kepala Sekolah")
<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="card-body">
        <h5 class="card-title mb-4">Log Surat</h5>
        <div height="100px" style="overflow:scroll; padding:10px; border-radius: 3px" class="bg-light text-dark">
          @foreach($log as $logger)
            Judul: <b>{{ $logger->mail_subject }}</b> - Oleh: <b>{{ App\User::where('id', $logger->mail_from)->value('name') }}</b>  - Untuk: <b>{{ App\User::where('id', $logger->mail_to)->value('name') }}</b> - Dibuat pada: {{ $logger->created_at }} <br>
          @endforeach
        </div>
      </div>
    </div>
  </div>
</div>
@endif
@endsection
