<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>SIAS - @yield('title')</title>
  <link rel="stylesheet" href="{{ asset('node_modules/font-awesome/css/font-awesome.min.css') }}" />
  <link rel="stylesheet" href="{{ asset('node_modules/perfect-scrollbar/dist/css/perfect-scrollbar.min.css') }}" />
  <link rel="stylesheet" href="{{ asset('node_modules/flag-icon-css/css/flag-icon.min.css') }}" />
  <link rel="stylesheet" href="{{ asset('css/style.css') }}" />
  <link rel="shortcut icon" href="{{ asset('images/favicon.png') }}" />
</head>

<body>
  <div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
    <nav class="navbar navbar-default col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
      <div class="bg-white text-center navbar-brand-wrapper">
        <a class="navbar-brand brand-logo" width="100px" href="{{ url('/dashboard') }}"><img src="{{ asset('images/logo.svg') }}" /></a>
        <a class="navbar-brand brand-logo-mini" href="{{ url('/dashboard') }}"><img src="{{ asset('images/logo_mini.svg') }}" alt=""></a>
      </div>
      <div class="navbar-menu-wrapper d-flex align-items-center">
        <button class="navbar-toggler navbar-toggler d-none d-lg-block navbar-dark align-self-center mr-3" type="button" data-toggle="minimize">
          <span class="navbar-toggler-icon"></span>
        </button>

        <ul class="navbar-nav ml-lg-auto d-flex align-items-center flex-row">
          <li class="nav-item">
            <a class="nav-link" href="{{ route('logout') }}"
            onclick="event.preventDefault();
            document.getElementById('logout-form').submit();">
            <i class="fa fa-power-off"></i>
          </a>

          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
          </form>
        </li>
      </ul>
      <button class="navbar-toggler navbar-dark navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
        <span class="navbar-toggler-icon"></span>
      </button>
    </div>
  </nav>

  <!-- partial -->
  <div class="container-fluid">
    <div class="row row-offcanvas row-offcanvas-right">
      <!-- partial:partials/_sidebar.html -->
      <nav class="sidebar sidebar-offcanvas" id="sidebar">
        <a class="profile" href="{{ route('profile') }}">
          <div class="user-info">
            <img src="{{ asset('files/'.Auth::user()->photo) }}" alt="">
            <p class="name">{{ Auth::user()->name }}</p>
            <p class="designation">{{ Auth::user()->level }}</p>
          </div>
        </a>
        <ul class="nav">
          <li class="nav-item">
            <a class="nav-link" href="{{ url('dashboard') }}">
              <img src="{{ asset('images/icons/1.png') }}" alt="">
              <span class="menu-title">Dashboard <span class="badge badge-primary"><b>{{ App\Mail::where(['mail_to' => Auth::id(), 'mark' => "unread"])->count() + App\Disposition::where(['mail_to' => Auth::id(), 'mark' => "unread"])->count() }}</b></span> </span>
            </a>
          </li>
          @if(Auth::user()->level == "Tata Usaha" || Auth::user()->level == "Kepala Sekolah")
          <li class="nav-item">
            <a class="nav-link" href="{{ url('surat/masuk') }}">
              <img src="{{ asset('images/icons/2.png') }}" alt="">
              <span class="menu-title">Surat Masuk</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ url('surat/keluar') }}">
              <img src="{{ asset('images/icons/003-outbox.png') }}" alt="">
              <span class="menu-title">Surat Keluar</span>
            </a>
          </li>
          @endif
          <li class="nav-item">
            <a class="nav-link" href="{{ url('disposisi/masuk') }}">
              <img src="{{ asset('images/icons/3.png') }}" alt="">
              <span class="menu-title">Disposisi</span>
            </a>
          </li>
          @if(Auth::user()->level == "Tata Usaha")
          <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#sample-pages" aria-expanded="false" aria-controls="sample-pages">
              <img src="{{ asset('images/icons/5.png') }}" alt="">
              <span class="menu-title">Entri Data<i class="fa fa-sort-down"></i></span>
            </a>
            <div class="collapse" id="sample-pages">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item">
                  <a class="nav-link" href="{{ route('anggota') }}">
                    Anggota
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="{{ route('jenis') }}">
                    Jenis Surat
                  </a>
                </li>
              </ul>
            </div>
          </li>
          @endif
          <li class="nav-item">
            <a class="nav-link" href="{{ url('laporan') }}">
              <img src="{{ asset('images/icons/6.png') }}" alt="">
              <span class="menu-title">Laporan</span>
            </a>
          </li>
        </ul>
      </nav>

      <!-- partial -->
      <div class="content-wrapper">
        @yield('content')
        @if(Auth::user()->level == "Tata Usaha")
        <a href="{{route('kirim_surat')}}" class="terbang btn btn-primary text-white btn-lg"><i class="fa fa-plus"></i> &nbsp;  Kirim Surat</a>
        @endif
      </div>
      <!-- partial:partials/_footer.html -->
      <footer class="footer">
        <div class="container-fluid clearfix">
          <span class="float-right">
            <a href="{{ url('dashboard') }}">SIAS - Sistem Informasi Arsip Surat</a> &copy; 2017
          </span>
        </div>
      </footer>

      <!-- partial -->
    </div>
  </div>

</div>

<script src="{{ asset('node_modules/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ asset('node_modules/popper.js/dist/umd/popper.min.js') }}"></script>
<script src="{{ asset('node_modules/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('node_modules/perfect-scrollbar/dist/js/perfect-scrollbar.jquery.min.js') }}"></script>
<script src="{{ asset('node_modules/chart.js/dist/Chart.min.js') }}"></script>
<script src="{{ asset('js/off-canvas.js') }}"></script>
<script src="{{ asset('js/hoverable-collapse.js') }}"></script>
<script src="{{ asset('js/misc.js') }}"></script>
<script src="{{ asset('js/tinymce/tinymce.min.js') }}"></script>
<script type="text/javascript">
tinymce.init({
  selector: "textarea",
  document_base_url: '{{ url('/') }}',
  plugins: [
    "advlist autolink autosave link image lists charmap print preview hr anchor pagebreak",
    "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
    "table contextmenu directionality emoticons template textcolor paste fullpage textcolor colorpicker textpattern"
  ],

  toolbar1: "newdocument fullpage | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect fontselect fontsizeselect",
  toolbar2: "cut copy paste | searchreplace | bullist numlist | outdent indent blockquote | undo redo | link unlink anchor image media code | insertdatetime preview | forecolor backcolor",
  toolbar3: "table | hr removeformat | subscript superscript | charmap emoticons | print fullscreen | ltr rtl | visualchars visualblocks nonbreaking template pagebreak restoredraft",

  menubar: false,
  toolbar_items_size: 'small',

  style_formats: [{
    title: 'Bold text',
    inline: 'b'
  }, {
    title: 'Red text',
    inline: 'span',
    styles: {
      color: '#ff0000'
    }
  }, {
    title: 'Red header',
    block: 'h1',
    styles: {
      color: '#ff0000'
    }
  }],

  init_instance_callback: function () {
    window.setTimeout(function() {
      $("#div").show();
     }, 1000);
  }
});

$(function() {
  'use strict';

  var SuratMasuk = {
    labels: ["{{ date('d')-5 }}", "{{ date('d')-4 }}", "{{ date('d')-3 }}", "{{ date('d')-2 }}", "{{ date('d')-1 }}", "{{ date('d') }}"],
    datasets: [{
      label: '# Surat Masuk',
      data: ["{{ App\Mail::where(DB::raw('DAY(created_at)'), date('d')-5)->where('mail_to', Auth::user()->id)->count() }}", "{{ App\Mail::where(DB::raw('DAY(created_at)'), date('d')-4)->where('mail_to', Auth::user()->id)->count() }}", "{{ App\Mail::where(DB::raw('DAY(created_at)'), date('d')-3)->where('mail_to', Auth::user()->id)->count() }}", "{{ App\Mail::where(DB::raw('DAY(created_at)'), date('d')-2)->where('mail_to', Auth::user()->id)->count() }}", "{{ App\Mail::where(DB::raw('DAY(created_at)'), date('d')-1)->where('mail_to', Auth::user()->id)->count() }}", "{{ App\Mail::where(DB::raw('DAY(created_at)'), date('d'))->where('mail_to', Auth::user()->id)->count() }}"],
      backgroundColor: [
        'rgba(255, 99, 132, 0.2)',
        'rgba(54, 162, 235, 0.2)',
        'rgba(255, 206, 86, 0.2)',
        'rgba(75, 192, 192, 0.2)',
        'rgba(153, 102, 255, 0.2)',
        'rgba(255, 159, 64, 0.2)'
      ],
      borderColor: [
        'rgba(255,99,132,1)',
        'rgba(54, 162, 235, 1)',
        'rgba(255, 206, 86, 1)',
        'rgba(75, 192, 192, 1)',
        'rgba(153, 102, 255, 1)',
        'rgba(255, 159, 64, 1)'
      ],
      borderWidth: 1
    }]
  };
  var SuratKeluar = {
    labels: ["{{ date('d')-5 }}", "{{ date('d')-4 }}", "{{ date('d')-3 }}", "{{ date('d')-2 }}", "{{ date('d')-1 }}", "{{ date('d') }}"],
    datasets: [{
      label: '# Surat Keluar',
      data: ["{{ App\Mail::where(DB::raw('DAY(created_at)'), date('d')-5)->where('mail_from', Auth::user()->id)->count() }}", "{{ App\Mail::where(DB::raw('DAY(created_at)'), date('d')-4)->where('mail_from', Auth::user()->id)->count() }}", "{{ App\Mail::where(DB::raw('DAY(created_at)'), date('d')-3)->where('mail_from', Auth::user()->id)->count() }}", "{{ App\Mail::where(DB::raw('DAY(created_at)'), date('d')-2)->where('mail_from', Auth::user()->id)->count() }}", "{{ App\Mail::where(DB::raw('DAY(created_at)'), date('d')-1)->where('mail_from', Auth::user()->id)->count() }}", "{{ App\Mail::where(DB::raw('DAY(created_at)'), date('d'))->where('mail_from', Auth::user()->id)->count() }}"],
      backgroundColor: [
        'rgba(255, 99, 132, 0.2)',
        'rgba(54, 162, 235, 0.2)',
        'rgba(255, 206, 86, 0.2)',
        'rgba(75, 192, 192, 0.2)',
        'rgba(153, 102, 255, 0.2)',
        'rgba(255, 159, 64, 0.2)'
      ],
      borderColor: [
        'rgba(255,99,132,1)',
        'rgba(54, 162, 235, 1)',
        'rgba(255, 206, 86, 1)',
        'rgba(75, 192, 192, 1)',
        'rgba(153, 102, 255, 1)',
        'rgba(255, 159, 64, 1)'
      ],
      borderWidth: 1
    }]
  };
  var options = {
    scales: {
      yAxes: [{
        ticks: {
          beginAtZero: true
        }
      }]
    },
    legend: {
      display: false
    },
    elements: {
      point: {
        radius: 0
      }
    }

  };
  if ($("#SuratMasuk").length) {
    var barChartCanvas = $("#SuratMasuk").get(0).getContext("2d");
    var barChart = new Chart(barChartCanvas, {
      type: 'bar',
      data: SuratMasuk,
      options: options
    });
  }
  if ($("#SuratKeluar").length) {
    var barChartCanvas = $("#SuratKeluar").get(0).getContext("2d");
    var barChart = new Chart(barChartCanvas, {
      type: 'bar',
      data: SuratKeluar,
      options: options
    });
  }
});
</script>
</body>
</html>
