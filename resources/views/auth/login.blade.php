@extends('layouts.auth')
@section('title', 'Masuk')

@section('content')
<div class="card-body px-5 py-5">
  <center>
    <img src="{{ asset('images/logo.svg') }}" class="img-responsive" alt="GhostMail" style="max-width:250px">
  </center>
  <h3 class="card-title text-left mt-4 mb-3">Masuk</h3>
  <form method="POST" action="{{ route('login') }}">
    {{ csrf_field() }}

    <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
      <input type="text" name="username" class="form-control p_input" placeholder="Username">
      @if ($errors->has('email'))
          <span class="help-block">
              <strong>{{ $errors->first('username') }}</strong>
          </span>
      @endif
    </div>
    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
      <input type="password" name="password" class="form-control p_input" placeholder="Password">
      @if ($errors->has('password'))
          <span class="help-block">
              <strong>{{ $errors->first('password') }}</strong>
          </span>
      @endif
    </div>
    <div class="text-center">
      <button type="submit" class="btn btn-primary btn-block enter-btn">Masuk</button>
    </div>
  </form>
</div>
@endsection
