@extends('layouts.auth')

@section('title', 'Daftar')

@section('content')

<div class="card-body px-5 py-5">
  <h3 class="card-title text-left mb-3">Daftar</h3>
  <form method="POST" action="{{ route('register') }}">
    {{ csrf_field() }}
    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
      <input type="text" name="name" class="form-control p_input" placeholder="Nama Lengkap">
      @if ($errors->has('name'))
          <span class="help-block text-danger">
              <p>{{ $errors->first('name') }}</p>
          </span>
      @endif
    </div>
    <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
      <input type="text" name="username" class="form-control p_input" placeholder="Username">
      @if ($errors->has('username'))
          <span class="help-block text-danger">
              <p>{{ $errors->first('username') }}</p>
          </span>
      @endif
    </div>
    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
      <input type="password" for="password" name="password" class="form-control p_input" placeholder="Password">
      @if ($errors->has('password'))
          <span class="help-block text-danger">
              <p>{{ $errors->first('password') }}</p>
          </span>
      @endif
    </div>
    <div class="form-group">
      <input type="password" class="form-control p_input" placeholder="Repeat Password"  name="password_confirmation" required>
    </div>
    <div class="text-center">
      <button type="submit" class="btn btn-primary btn-block enter-btn">Daftar</button>
    </div>
    <p class="existing-user text-center pt-4 mb-0">Sudah mempunyai akun?&nbsp;<a href="{{ url('/login') }}">Masuk</a></p>
  </form>
</div>
@endsection
